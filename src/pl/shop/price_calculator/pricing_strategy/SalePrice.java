package pl.shop.price_calculator.pricing_strategy;

public class SalePrice implements PricingStrategy {
    @Override
    public void calculatePrice(int price, boolean isSignedUpForNewsletter) {
        if(isSignedUpForNewsletter==true){
            System.out.printf(String.valueOf(price*0.5));
        }else {
            System.out.println("Nie jesteś zapisany do newslettera");
        }
    }
}
