package pl.shop.price_calculator.pricing_strategy;

public class RegularPrice implements PricingStrategy{
    @Override
    public void calculatePrice(int price, boolean isSignedUpForNewsletter) {
        if(isSignedUpForNewsletter==false){
            System.out.printf(String.valueOf(price));
        }else {
            System.out.println("Jesteś zapisany do newslettera");
        }

    }
}
